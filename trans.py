#!/usr/bin/python3
import argparse
from googletrans import Translator
import sys
import os
import mariadb

if os.getenv('SUDO_USER') is None:
	print('Script can not be started directly', file=sys.stderr)
	sys.exit(1)
elif os.getenv('SUDO_USER', 'www-data') == 'www-data' or os.getenv('SUDO_UID', 33) == 33:
	if os.getenv('SERVER_PORT') is None:
		print('www-data is not allowed to start this script directly', file=sys.stderr)
		sys.exit(1)
	elif os.getenv('SERVER_PORT', 0) != '8080':
		print('403 - Forbidden')
		print(f'403 - Forbidden, Port {os.environ["SERVER_PORT"]}', file=sys.stderr)
		sys.exit(1)

translator = Translator()

parser = argparse.ArgumentParser(
	prog='DMP Translator'
	,description='Translate input via Google Translator and print translation'
)
parser.add_argument('dest', help='Language to which the input text should be translated')
parser.add_argument('src', help='Language from which the input text should be translated')
parser.add_argument('aname', help='Name of the content area')
parser.add_argument('pid', help='PageID of the page reference')
parser.add_argument('--dev', action='store_true', help='Access the dev tables')
args = parser.parse_args()

condata = {
	'server' : None
	, 'user' : None
	, 'password' : None
	, 'DB' : None
}

for line in sys.stdin:
	kv = line.strip('\n')
	key, value = kv.split('=')
	if not condata.get(key, False) is bool:
		condata[key] = value

con = None
cur = None
try:
	con = mariadb.connect(host=condata['server'], user=condata['user'], password=condata['password'], database=condata['DB'])
	cur = con.cursor(buffered=True)
	query = "SELECT TXT_BODY FROM displaylang{dev} where Area='{aname}' and lang='{from_lang}' AND PageID = {pid}".format(
		dev='' if not args.dev else 'DEV'
		, aname=args.aname
		, from_lang=args.src
		, pid=args.pid
		)
	cur.execute(query)
	intxt = cur.fetchone()[0]
except Exception as e:
	print(e, file=sys.stderr)
	sys.exit(1)
finally:
	if cur is not None:
		cur.close()
	if con is not None:
		con.close()

trans = translator.translate(intxt, src=args.src, dest=args.dest)

if trans.text[0:2] == "b'" and trans.text[-1:] == "'":
	e_win = trans.text.encode('cp1252')
	unic = e_win.decode('latin-1')
	print(unic[2:-1].replace(r'\r', '').replace(r'\n', '\n')) # use raw-str for replace __repr__ of new lines with special chars
else:
	e_utf = trans.text.encode('utf-8')
	unic = e_utf.decode('latin-1')
	print(unic)